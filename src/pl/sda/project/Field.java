package pl.sda.project;

import java.util.ArrayList;

public class Field {
    private ArrayList<Integer> ability = new ArrayList<>();
    private int value=0;

    Field() {
        for (int i = 1; i < 10; i++) {
            ability.add(i);
        }
    }

    public void check() {
        if (ability.size() == 1) {
            this.setValue(ability.get(0));
        }
    }
    public  void setValue(int i){
        this.value = i;
        if(ability.size()>0)ability.clear();
    }
    public int getValue() {
        return  this.value;
    }

    public  void remove( Integer i ){
        if(ability.contains(i)) ability.remove(i);
    }
    public  boolean isAbility(Integer i) {
        return this.ability.contains(i);
    }

}



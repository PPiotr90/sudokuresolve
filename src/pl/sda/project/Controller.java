package pl.sda.project;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;


public class Controller {
    @FXML
    GridPane mainGrid;
    @FXML
    GridPane solutionGrid;
    @FXML
    Button button;
    Field[] fields = new Field[81];
    TextField[] textFields = new TextField[81];
    Label[] labels = new Label[81];

    public void initialize() {

        mainGrid.setGridLinesVisible(true);
        solutionGrid.setGridLinesVisible(true);
        button.setText("Resolve");


        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                TextField textField = new TextField();
                Label label = new Label();
                mainGrid.add(textField, i, j);
                textFields[i + 9 * j] = textField;
                solutionGrid.add(label, i, j);
                labels[i + 9 * j] = label;
                Field field = new Field();
                fields[i + 9 * j] = field;
            }
        }
                textFields[0].setText("5");
                textFields[1].setText("3");
                textFields[9].setText("6");
                textFields[19].setText("9");
                textFields[20].setText("8");
                textFields[4].setText("7");
                textFields[12].setText("1");
                textFields[13].setText("9");
                textFields[14].setText("5");
                textFields[25].setText("6");
                textFields[27].setText("8");
                textFields[36].setText("4");
                textFields[45].setText("7");
                textFields[35].setText("3");
                textFields[44].setText("1");
                textFields[53].setText("6");
                textFields[61].setText("8");
                textFields[60].setText("2");
                textFields[71].setText("5");
                textFields[80].setText("9");
                textFields[79].setText("7");
                textFields[68].setText("9");
                textFields[67].setText("1");
                textFields[66].setText("4");
                textFields[76].setText("8");
                textFields[49].setText("2");
                textFields[39].setText("8");
                textFields[41].setText("3");
                textFields[31].setText("6");
                textFields[55].setText("6");




        button.setOnAction(event -> {
            this.setValues();
            this.checkAll();
            this.checkIsOnlyOne();
        });


    }

    public void setValues() {
        for (int i = 0; i < 81; i++) {
            if (!(textFields[i].getText().isEmpty())) {
                int value = Integer.parseInt(textFields[i].getText());
                fields[i].setValue(value);
                System.out.println("set "+i+"  value: "+value);
                this.removeOthers(i, value);
            }

        }

    }

    public void clearSquare(int i, int value) {
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
                fields[i + k + 9 * j].remove(value);
            }
        }
    }

    public void checkAll() {
        for (int i = 0; i < 81; i++) {
            fields[i].check();
            if (fields[i].getValue() > 0) {
                labels[i].setText(Integer.toString(fields[i].getValue()));
                this.removeOthers(i, fields[i].getValue());
            }
        }
    }

    public void checkIsOnlyOne() {
        for (int i = 1; i <= 9; i++) {
            //for all columns
            for (int j = 0; j <= 8; j++) {
                int counter = 0;
                int position = 0;
                for (int k = j; k < 81; k += 9) {
                    if (fields[k].isAbility(i)) {
                        counter++;
                        position = k;
                    }
                    if (counter > 1) break;
                }
                if (counter == 1) {
                    fields[position].setValue(i);
                    System.out.println("set "+position+"  value: "+i);
                    this.removeOthers(position, i);
                }
            }
            //for all rows
            for (int j = 0; j < 81; j += 9) {
                int counter = 0;
                int position = 0;
                for (int k = j; k < j + 9; k++) {
                    if (fields[k].isAbility(i)) {
                        counter++;
                        position = k;
                    }
                    if (counter > 1) break;
                }
                if (counter == 1) {
                    fields[position].setValue(i);
                    System.out.println("set "+position+"  value: "+i);
                    this.removeOthers(position, i);
                }
            }
            //for all small squares
            for (int j = 0; j < 8; j += 3) {
                for (int k = j; k < 81; k+=27) {
                    int counter=0;
                    int position=0;
                    for (int l = k; l < k+3; l++) {

                        for (int m = l; m < l+27; m+=9) {
                            if (fields[m].isAbility(i)) {
                                counter++;
                                position = m;
                            }
                        }
                            if (counter > 1) break;
                        }
                        if (counter == 1) {
                            fields[position].setValue(i);
                            System.out.println("set "+position+"  value: "+i);
                            this.removeOthers(position, i);

                        }



                }


                    }
                }
            }


    public void removeOthers(int i, int value) {
        // remove all in column
        for (int j = i % 9; j < 81; j += 9) {
            fields[j].remove(value);
        }
        //remove all in raw
        int t = i - i % 9;
        for (int j = t; j < t + 9; j++) {
            fields[j].remove(value);
        }
        // remove from all small squares
        int k =i-i%3;
        int p = k-(((int)(i/9))%3)*9;
        this.clearSquare(p, value);
    }
}

